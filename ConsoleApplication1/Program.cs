﻿using System;

namespace ConsoleApplication1
{
  class Program
  {
    static void Main(string[] args)
    {
      // default empty constructor will generate a iPhone 5 object not assigned to user
      SmartPhone myPhone = new SmartPhone();

      // "none" for make and model indicates the person does not have a phone
      SmartPhone CeCePhone = new SmartPhone("CeCe", "none", "none");

      // call with make and model for phones known about 
      SmartPhone AmyPhone = new SmartPhone("Amy","Apple", "iPhone 5 16 GB");
      SmartPhone DillonPhone = new SmartPhone("Dillon","Samsung", "Galaxy III 16 GB");
      
      // create object with simple constructor, but specs not known by system
      SmartPhone BarbaraPhone = new SmartPhone("Barbara","Nokia", "xx8989");

      // create object with complex constructor and pass in all data for iPhone 6 Plus
      SmartPhone ShelleyPhone = new SmartPhone("Shelley", "Apple", "iPhone 6 Plus 64 GB", 5.5, "1080 x 1920", 8,
                                                 1.2, 6.22, 3.06, 2915, "Dual-core 1.4 GHz Cyclone", 64);

      myPhone.PrintUserPhMakeModel();
      myPhone.PrintSpecs();
      myPhone.PrintSmartSpecs();

      AmyPhone.PrintUserPhMakeModel();
      AmyPhone.PrintSpecs();
      AmyPhone.PrintSmartSpecs();

      DillonPhone.PrintUserPhMakeModel();
      DillonPhone.PrintSpecs();
      DillonPhone.PrintSmartSpecs();

      CeCePhone.PrintUserPhMakeModel();
      CeCePhone.PrintSpecs();
      CeCePhone.PrintSmartSpecs();

      BarbaraPhone.PrintUserPhMakeModel();
      BarbaraPhone.PrintSpecs();
      BarbaraPhone.PrintSmartSpecs();

      ShelleyPhone.PrintUserPhMakeModel();
      ShelleyPhone.PrintSpecs();
      ShelleyPhone.PrintSmartSpecs();    

      Console.WriteLine("Type any key to exit.");
      Console.ReadKey();
    }
  }
}
