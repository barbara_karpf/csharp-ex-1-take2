﻿using System;

namespace ConsoleApplication1
{
  public class Phone
  {
    public string user;
    public string make;
    public string model;
    public double? length;
    public double? width;
    public int? battCapacity_mAh;
    public bool specs_exist;

    // empty constructor initializes SmartPhone object as an iPhone 4 8 GB with no user
    public Phone()
    {
      user = "Unassigned";
      make = "Apple";
      model = "iPhone 4 8 GB";
      length = 4.54;
      width = 2.31;
      battCapacity_mAh = 1420;
      specs_exist = true;
    }

    // if it is a known type of phone, can call constructor with only make and model
    public Phone(string phUser, string phMake, string phModel)
    {
      user = phUser;
      make = phMake;
      model = phModel;
      specs_exist = false;

      switch (make)
      {
        case "Apple":
          if (model == "iPhone 5 16 GB")
          {
            specs_exist = true;
            length = 4.87;
            width = 2.31;
            battCapacity_mAh = 1440;
          }
          break;

        case "Samsung":
          if (model == "Galaxy III 16 GB")
          {
            specs_exist = true;
            length = 5.4;
            width = 2.8;
            battCapacity_mAh = 2100;
          }
          break;

        default:
          // note specs_exist stays set to false
          length = null;
          width = null;
          battCapacity_mAh = null;
          break;
      }
    }

    // if unknown type of phone need to construct with all information
    public Phone(string phUser, string phMake, string phModel,
                      double phLength, double phWidth, int phBattCap_mAh)
    {
      user = phUser;
      make = phMake;
      model = phModel;
      length = phLength;
      width = phWidth;
      battCapacity_mAh = phBattCap_mAh;
      specs_exist = true;
    }

    public void PrintSpecs()
    {
      if (this.make == "none")
      {
        Console.WriteLine("There are no specs for this phone as this person doesn't have one.\n");
      }
      else if (!this.specs_exist)
      {
        Console.WriteLine("Sorry! We don't have the BASIC specs for this phone in our system.\n");
      }
      else
      {
        Console.WriteLine("The BASIC specs for this phone are as follows:  \n" +
                                                                      "\t Dimensions: {0} in x {1} in\n" +
                                                                      "\t Battery capacity: {2} mAh\n",
                                                                      this.length,
                                                                      this.width,
                                                                      this.battCapacity_mAh);
      };
    } // end of method PrintSpecs

    public void PrintUserPhMakeModel()
    {
      Console.WriteLine("=============================================================================");
      Console.WriteLine("\n{0}'s phone is made by {1} and is model {2}.\n", this.user, this.make, this.model);
    } // end of method PrintUserPhMakeModel

  } // end of class Phone

} // end of Namespace



