﻿using System;

namespace ConsoleApplication1
{
  public class SmartPhone : Phone
  {
    public double? screenSize_In;
    public string screenResPx;
    public double? frontCameraMpx;
    public double? backCameraMpx;
    public string processor;
    public int? storageGB;

    // empty constructor initializes SmartPhone object as a 8 GB iPhone 4 with no user
    public SmartPhone () : base ()
    {
      screenSize_In = 3.5;
      screenResPx = "640 x 960";
      frontCameraMpx = 5;
      backCameraMpx = 0;
      processor = "1 GHz Cortex-A8";
      storageGB = 8;
    }

    // if it is a known type of phone, can call constructor with only user, make and model
    public SmartPhone(string phUser, string phMake, string phModel) : base (phUser, phMake, phModel)
    {
      switch (base.make)
      {
        case "Apple":
          if (model == "iPhone 5 16 GB")
          {
            screenSize_In = 4.0;
            screenResPx = "640 x 1136";
            frontCameraMpx = 8;
            backCameraMpx = 1.2;
            processor = "Dual-core 1.3 GHz Swift";
            storageGB = 16;
          }
          break;

        case "Samsung":
          if (model == "Galaxy III 16 GB")
          {
            screenSize_In = 4.8;
            screenResPx = "720 x 1280";
            frontCameraMpx = 8;
            backCameraMpx = 2;
            processor = "Dual-core 1.5 GHz";
            storageGB = 16;
          }
          break;

        default:
          screenSize_In = null;
          screenResPx = "";
          frontCameraMpx = null;
          backCameraMpx = null;
          processor = "";
          storageGB = null;
          break;
      }
    } // end of constructor using only user, make, model

    // if unknown type of phone, need to construct with all information
    public SmartPhone(string phUser, string phMake, string phModel, double phScrnSizIn,
                      string phScreenResPx, double phFrontCamMpx, double phBackCamMpx, 
                      double phLength, double phWidth, int phBattCap_mAh, string phProcessor, int phStorageGB) :
           base(phUser, phMake, phModel, phLength, phWidth, phBattCap_mAh)
    {
          screenSize_In = phScrnSizIn;
          screenResPx = phScreenResPx;
          frontCameraMpx = phFrontCamMpx;
          backCameraMpx = phBackCamMpx;
          processor = phProcessor;
          storageGB = phStorageGB;
    }

    public void PrintSmartSpecs()
    {
      if (this.make != "none")
      {
          if (!this.specs_exist) 
          {
            Console.WriteLine("Sorry! We don't have the SMART PHONE specs for this phone in our system.\n");
          }
          else
          {
            Console.WriteLine("The SMART PHONE specs for this phone are as follows:  \n\t Screen size: {0} in\n" +
                                                                          "\t Screen resolution: {1} (pixels)\n" +
                                                                          "\t Front camera: {2} MPx\n" +
                                                                          "\t Back camera: {3} MPx\n" +
                                                                          "\t Processor: {4}\n" +
                                                                          "\t Storage space: {5} GB\n",
                                                                          this.screenSize_In,
                                                                          this.screenResPx,
                                                                          this.frontCameraMpx,
                                                                          this.backCameraMpx,
                                                                          this.processor,
                                                                          this.storageGB);
          }
      }
    } // end of method PrintSmartSpecs

  } // end of class SmartPhone

} // end of namespace
